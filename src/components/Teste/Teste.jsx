import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './Teste.styles';

class Teste extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Teste will mount');
  }

  componentDidMount = () => {
    console.log('Teste mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Teste will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Teste will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Teste did update');
  }

  componentWillUnmount = () => {
    console.log('Teste will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="TesteWrapper">
        Test content
      </div>
    );
  }
}

Teste.propTypes = {
  // bla: PropTypes.string,
};

Teste.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Teste);

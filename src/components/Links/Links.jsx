import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Links.styles';

class Links extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      links: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Links will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Links will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Links will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Links did update');
  }

  componentWillUnmount = () => {
    console.log('Links will unmount');
  }

  componentDidMount = () => {
    console.log('Links mounted');
    fetch('/links')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          links: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { links, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Links</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          links.map(link => {
            const { access_mode, created_by, id, institution, last_accessed_at, status } = link;
            return (
              <div key={id} className='LinksWrapper'>
                <ul>
                  <li>ID: {id}</li>
                  <li>Created By: {created_by}</li>
                  <li>Access Mode: {access_mode}</li>
                  <li>Institution: {institution}</li>
                  <li>Last Access: {last_accessed_at}</li>
                  <li>Status: {status}</li>
                </ul>
              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Links.propTypes = {
  // bla: PropTypes.string,
};

Links.defaultProps = {
  // bla: 'test',
};

export default Links;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Transactions.styles';

class Transactions extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      transactions: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Transactions will mount');
    
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Transactions will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Transactions will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Transactions did update');
  }

  componentWillUnmount = () => {
    console.log('Transactions will unmount');
  }

  componentDidMount = () => {
    console.log('Transactions mounted');
    fetch('/transactions')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          transactions: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { transactions, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Transactions</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          transactions.map(transaction => {
            const { oi } = transaction;

            return (
              <div key={1} className='TransactionsWrapper'>
           
              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Transactions.propTypes = {
  // bla: PropTypes.string,
};

Transactions.defaultProps = {
  // bla: 'test',
};

export default Transactions;

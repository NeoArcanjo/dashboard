import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Statements.styles';

class Statements extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      statements: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Statements will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Statements will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Statements will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Statements did update');
  }

  componentWillUnmount = () => {
    console.log('Statements will unmount');
  }

  componentDidMount = () => {
    console.log('Statements mounted');
    fetch('/statements')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          statements: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { statements, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Statements</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          statements.map(statement => {
            const { oi } = statement;

            return (
              <div key={1} className='StatementsWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Statements.propTypes = {
  // bla: PropTypes.string,
};

Statements.defaultProps = {
  // bla: 'test',
};

export default Statements;

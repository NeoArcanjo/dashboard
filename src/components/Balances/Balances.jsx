import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Balances.styles';

class Balances extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      balances: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Balances will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Balances will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Balances will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Balances did update');
  }

  componentWillUnmount = () => {
    console.log('Balances will unmount');
  }

  componentDidMount = () => {
    console.log('Balances mounted');
    fetch('/balances')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          balances: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { balances, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Balances</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          balances.map(balance => {
            const { oi } = balance;
            console.log(balance)
            return (
              <div key={1} className='BalancesWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Balances.propTypes = {
  // bla: PropTypes.string,
};

Balances.defaultProps = {
  // bla: 'test',
};

export default Balances;

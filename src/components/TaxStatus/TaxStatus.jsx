import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './TaxStatus.styles';

class TaxStatus extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      taxStatus: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('TaxStatus will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('TaxStatus will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('TaxStatus will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('TaxStatus did update');
  }

  componentWillUnmount = () => {
    console.log('TaxStatus will unmount');
  }

  componentDidMount = () => {
    console.log('TaxStatus mounted');
    fetch('/taxstatus')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          taxstatus: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { taxstatus, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>TaxStatus</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          taxstatus.map(taxstats => {
            const { oi } = taxstats;

            return (
              <div key={1} className='TaxStatusWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

TaxStatus.propTypes = {
  // bla: PropTypes.string,
};

TaxStatus.defaultProps = {
  // bla: 'test',
};

export default TaxStatus;

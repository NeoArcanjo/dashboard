import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Accounts.styles';

class Accounts extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      accounts: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Accounts will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Accounts will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Accounts will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Accounts did update');
  }

  componentWillUnmount = () => {
    console.log('Accounts will unmount');
  }

  componentDidMount = () => {
    console.log('Accounts mounted');
    fetch('/accounts')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          accounts: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { accounts, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Accounts</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          accounts.map(account => {
            const { oi } = account;
            console.log(account)

            return (
              <div key={1} className='AccountsWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Accounts.propTypes = {
  // bla: PropTypes.string,
};

Accounts.defaultProps = {
  // bla: 'test',
};

export default Accounts;

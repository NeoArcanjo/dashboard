import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Owners.styles';

class Owners extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      owners: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Owners will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Owners will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Owners will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Owners did update');
  }

  componentWillUnmount = () => {
    console.log('Owners will unmount');
  }

  componentDidMount = () => {
    console.log('Owners mounted');
    fetch('/owners')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          owners: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { owners, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Owners</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          owners.map(owner => {
            const { oi } = owner;

            return (
              <div key={1} className='OwnersWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Owners.propTypes = {
  // bla: PropTypes.string,
};

Owners.defaultProps = {
  // bla: 'test',
};

export default Owners;

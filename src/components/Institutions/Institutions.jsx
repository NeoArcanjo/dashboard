import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Institutions.styles';

class Institutions extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      institutions: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Institutions will mount');
  }


  componentWillReceiveProps = (nextProps) => {
    console.log('Institutions will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Institutions will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Institutions did update');
  }

  componentWillUnmount = () => {
    console.log('Institutions will unmount');
  }

  componentDidMount = () => {
    console.log('Institutions mounted');
    fetch('/institutions')
      .then(response => response.json())
      // ...then we update the users state
      .then(data =>
        {
          console.log(data)
        this.setState({
          institutions: data,
          isLoading: false,
        })
        }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }
  
  render() {
    const { institutions, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Institutions</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          institutions.map(institution => {
            const { code, country_codes, customization, display_name, form_fields, logo, name, primary_color, type, website } = institution;
            console.log(customization)

            return (
              <div key={display_name} className='InstitutionsWrapper'>
                <p>{code}</p>
                <ul>
                  <li>
                    {country_codes[0]}
                  </li>
                </ul>

                {/* <p>{customization}</p> */}
                <p>{display_name}</p>
                <ul>
                  <li>
                    {form_fields[0].label}
                  </li>
                  <li>
                    {form_fields[0].name}
                  </li>
                  <li>
                    {form_fields[0].placeholder}
                  </li>
                  <li>
                    {form_fields[0].type}
                  </li>
                  <li>
                    {form_fields[0].validation}
                  </li>
                  <li>
                    {form_fields[0].validation_message}
                  </li>
                </ul>

                <ul>
                  <li>
                    {form_fields[1].label}
                  </li>
                  <li>
                    {form_fields[1].name}
                  </li>
                  <li>
                    {form_fields[1].placeholder}
                  </li>
                  <li>
                    {form_fields[1].type}
                  </li>
                  <li>
                    {form_fields[1].validation}
                  </li>
                  <li>
                    {form_fields[1].validation_message}
                  </li>
                </ul>

                {/* <p>{institution.length}</p> */}
                <img src={logo} alt="logo" className="mr-2 rounded" style={{ width: 40, height: 40 }} data-holder-rendered="true" />
                <p>{name}</p>
                <p>{primary_color}</p>
                <p>{type}</p>
                <a href={website}> {website}</a>

                {/* <Button href={`/institutions/id/` + id} type="submit" className="btn btn-success">Detalhes</Button> */}

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Institutions.propTypes = {
  // bla: PropTypes.string,
};

Institutions.defaultProps = {
  // bla: 'test',
};

export default Institutions;

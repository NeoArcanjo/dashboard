import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './Invoices.styles';

class Invoices extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      invoices: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('Invoices will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Invoices will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Invoices will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('Invoices did update');
  }

  componentWillUnmount = () => {
    console.log('Invoices will unmount');
  }

  componentDidMount = () => {
    console.log('Invoices mounted');
    fetch('/invoices')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          invoices: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { invoices, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>Invoices</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          invoices.map(invoice => {
            const { oi } = invoice;
            console.log(invoice)

            return (
              <div key={1} className='InvoicesWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

Invoices.propTypes = {
  // bla: PropTypes.string,
};

Invoices.defaultProps = {
  // bla: 'test',
};

export default Invoices;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
//import { Test } from './TaxReturns.styles';

class TaxReturns extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      taxReturns: [],
      isLoading: true,
      error: null,
    };
  }

  componentWillMount = () => {
    console.log('TaxReturns will mount');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('TaxReturns will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('TaxReturns will update', nextProps, nextState);
  }

  componentDidUpdate = () => {
    console.log('TaxReturns did update');
  }

  componentWillUnmount = () => {
    console.log('TaxReturns will unmount');
  }

  componentDidMount = () => {
    console.log('TaxReturns mounted');
    fetch('/taxreturns')
      .then(response => response.json())
      // ...then we update the users state
      .then(data => {
        console.log(data)
        this.setState({
          taxreturns: data,
          isLoading: false,
        })
      }
      )
      // Catch any errors we hit and update the app
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render() {
    const { taxreturns, isLoading, error } = this.state;
    return (
      <React.Fragment>
        <h1>TaxReturns</h1>
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          taxreturns.map(taxreturn => {
            const { oi } = taxreturn;

            return (
              <div key={1} className='TaxReturnsWrapper'>

              </div>
            );
          })
          // If there is a delay in data, let's let the user know it's loading
        ) : (
            <h3>Loading...</h3>
          )}
      </React.Fragment>
    );
  }
}

TaxReturns.propTypes = {
  // bla: PropTypes.string,
};

TaxReturns.defaultProps = {
  // bla: 'test',
};

export default TaxReturns;

import React, { useState, useEffect } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';

function useRows() {
    const [rows, setRows] = useState([0])
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);

    function getRows() {
        setLoading(true);
        fetch('/institutions')
            .then(response => response.json())
            .then(data => {
                setRows(data)
                setLoading(false)
            })
            .catch(e => {
                setError(e);
            })//.finally(() => setLoading(false))
    }

    useEffect(() => {
        getRows();
    }, []);

    return [rows, error, loading];
}

function preventDefault(event) {
    event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
}));

// export default function Institutions() {
export default function Institutions() {
    const classes = useStyles();
    const [rows, error, loading] = useRows();

    console.log(rows)

    if (error) return "Failed to load resource";
    return loading ? "Loading..." : (
        <React.Fragment>
            <Title>List Institutions</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Logo</TableCell>
                        <TableCell>Code</TableCell>
                        <TableCell>Country Code</TableCell>
                        {/* <TableCell>customization</TableCell> */}
                        <TableCell>Display Name</TableCell>
                        {/* <TableCell align="right">form_fields</TableCell> */}
                        <TableCell>Name</TableCell>
                        <TableCell>Primary Color</TableCell>
                        <TableCell>Type</TableCell>
                        <TableCell>Website</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index}>
                            <TableCell><img src={row.logo} alt="logo" width='50px'></img></TableCell>
                            <TableCell>{row.code}</TableCell>
                            <TableCell>{row.country_codes}</TableCell>
                            {/* <TableCell>{row.customization}</TableCell> */}
                            <TableCell>{row.display_name}</TableCell>
                            {/* <TableCell align="right">{row.form_fields}</TableCell> */}
                            <TableCell>{row.name}</TableCell>
                            <TableCell>{row.primary_color}</TableCell>
                            <TableCell>{row.type}</TableCell>
                            <TableCell><a href={row.website}>{row.website}</a></TableCell> 
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <div className={classes.seeMore}>
                <Link color="primary" href="#" onClick={preventDefault}>
                    See more institutions
        </Link>
            </div>
        </React.Fragment>
    )
}

import React from 'react'
import { BrowserRouter } from "react-router-dom"
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
// import '../../node_modules/font-awesome/css/font-awesome.min.css'
import Routes from './routes'

export default function App() {
    return (
        <BrowserRouter>
            <Routes />
        </BrowserRouter>
    );
}

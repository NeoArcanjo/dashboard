import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import Links from '../components/Links/index.js'
import Accounts from '../components/Accounts/index.js'
import Transactions from '../components/Transactions/index.js'
import Balances from '../components/Balances/index.js'
import Owners from '../components/Owners/index.js'
import Institutions from '../components/Institutions/index.js'
import Invoices from '../components/Invoices/index.js'
import TaxReturns from '../components/TaxReturns/index.js'
import TaxStatus from '../components/TaxStatus/index.js'
import Statements from '../components/Statements/index.js'
import RouteNotFound from '../components/Notfound/index.js'
import Dashboard from '../dashboard/Dashboard.js'
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import Logo from '../logo.svg'

class Main extends React.Component {
    state = {
        searchText: ""
    };

    handleRoute = route => () => {
        this.props.history.push({ pathname: route });
    };

    handleSearchInput = event => {
        this.setState({
            searchText: event.target.value
        });
    };

    handleSearchSubmit = () => {
        if (this.state.searchText) {
            this.props.history.push({
                pathname: "/results",
                state: {
                    searchText: this.state.searchText
                }
            });
        } else {
            alert("Insira um termo para busca");
        }
    };

    render() {
        return (
            <>
                <Navbar className="navbar navbar-expand-md header fixed-top">
                    <a className="navbar-brand" href="/"><img src={Logo} alt='logo' width="70px" /></a>
                    <button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="true" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"><svg width={24} height={24} xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd">
                            <path style={{ fill: '#fff' }} d="M24 18v1h-24v-1h24zm0-6v1h-24v-1h24zm0-6v1h-24v-1h24z" fill="#fff" />
                            <path style={{ fill: '#fff' }} d="M24 19h-24v-1h24v1zm0-6h-24v-1h24v1zm0-6h-24v-1h24v1z" /></svg></span>
                    </button>

                    <div className="navbar-collapse collapse" id="navbarCollapse">
                        <Nav className="navbar-nav mr-auto">
                            <Nav.Link onClick={this.handleRoute("/accounts")}>Contas</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/balances")}>Balanços</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/institutions")}>Instituições</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/invoices")}>Invoices</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/links")}>Links</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/owners")}>Proprietários</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/statements")}>Statements</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/tax-returns")}>Tax returns</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/tax-status")}>Tax status</Nav.Link>
                            <Nav.Link onClick={this.handleRoute("/transactions")}>Transações</Nav.Link>
                        </Nav>

                        {/* <Form inline >
                            <FormControl
                                onChange={this.handleSearchInput}
                                value={this.state.searchText}
                                type="text"
                                placeholder="Buscar"
                                className="mr-sm-2"
                            />
                            <Button onClick={this.handleSearchSubmit} variant="outline-warning my-2 my-sm-0">
                                Buscar
                                </Button>
                        </Form> */}
                    </div>
                </Navbar>
                <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route exact path='/accounts' component={Accounts} />
                    <Route exact path='/balances' component={Balances} />
                    <Route exact path='/institutions' component={Institutions} />
                    <Route exact path='/invoices' component={Invoices} />
                    <Route exact path='/links' component={Links} />
                    <Route exact path='/owners' component={Owners} />
                    <Route exact path='/statements' component={Statements} />
                    <Route exact path='/tax-returns' component={TaxReturns} />
                    <Route exact path='/tax-status' component={TaxStatus} />
                    <Route exact path='/transactions' component={Transactions} />
                    <Route component={RouteNotFound} />
                </Switch>
            </>
        );
    }
}

export default withRouter(Main);

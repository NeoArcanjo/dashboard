# Use Guide #

Preparando o ambiente

``` shell
cd api
python3 -m venv venv
. venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
```

Levantando o servidor da API


``` shell 
yarn start-api
```

Levantando aplicação frontend React

``` shell
yarn start
```

from pprint import pprint
from belvo.client import Client
from flask import Flask, jsonify, redirect, request, url_for
import json

app = Flask(__name__)
# Login to Belvo API
api = json.load( open('./config-api.json') )
client = Client( api['id'] , api['password'], api['endpoint'] )

@app.route('/links', methods=['GET'])
def show_links():
    links = list(client.Links.list())
    return jsonify(links)

@app.route('/links', methods=['POST'])
def create_link():
    pprint(request)
    # link = client.Links.create(
    #     institution="banamex_mx_retail",
    #     username="johndoe",
    #     password="supersecret"
    # )
    # return jsonify(link)

@app.route('/accounts')
def show_accounts():
    accounts = list(client.Accounts.list())
    return jsonify(accounts)


@app.route('/transactions')
def show_transactions():
    transactions = list(client.Transactions.list())
    return jsonify(transactions)

@app.route('/balances')
def show_balances():
    balances = list(client.Balances.list())
    return jsonify(balances)

@app.route('/owners')
def show_owners():
    owners = list(client.Owners.list())
    return jsonify(owners)


@app.route('/institutions')
def show_institutions():
    institutions = list(client.Institutions.list())
    return jsonify(institutions)


@app.route('/invoices')
def show_invoices():
    invoices = list(client.Invoices.list())
    return jsonify(invoices)


@app.route('/tax-returns')
def show_tax_returns():
    tax_returns = list(client.TaxReturns.list())
    return jsonify(tax_returns)


@app.route('/tax-status')
def show_tax_status():
    tax_status = list(client.TaxStatus.list())
    return jsonify(tax_status)


@app.route('/statements')
def show_statements():
    statements = list(client.Statements.list())
    return jsonify(statements)

# # Register a link
# link = client.Links.create(
#     institution="banamex",
#     username="johndoe",
#     password="supersecret"
# )

# # Get all accounts
# # client.Accounts.create(link["id"])

# # Pretty print all checking accounts
# for account in client.Accounts.list(type="checking"):
#     pprint(account)

# for institution in client.Institutions.list():
#     pprint(institution)
